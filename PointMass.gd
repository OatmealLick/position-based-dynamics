tool
extends Node2D
class_name PointMass

var velocity          := Vector2( 0.0, 0.0 )
var previous_position := Vector2( 0.0, 0.0 )
var predicted_position := Vector2.ZERO
var debug := false

var mass              := 1.0 setget set_mass
var mu                := 1.0 # 1./mass
var neighbors         := []
var resting_length    := []

var damping := Vector2.ZERO
var debug_timer : Timer
var next_time_print := false

export(bool) var is_debug := false
export(bool) var is_static:bool = false setget set_static
func set_static(value):
	is_static = value
	if is_static:
		get_node("sprite").modulate = Color( 1.0, 0.0, 0.0 )
	else:
		get_node("sprite").modulate = Color( 1.0, 1.0, 1.0 )

func add_neighbor(point_mass):
	neighbors.append(point_mass)
	resting_length.append((point_mass.position - position).length())


func _ready():
	previous_position = position - velocity * get_physics_process_delta_time()
		
	
func _process(delta):
	update()


#func pre_constraint_update( delta ):
#	velocity = velocity + mu * globals.gravity * delta
#	predicted_position = position + velocity * delta
#
#func post_constraint_update(delta):
#	velocity = (predicted_position - position) / delta
#	position = predicted_position
#	velocity *= globals.medium_damping
	
	
func set_velocity(v):
	velocity          = v
	previous_position = position - velocity * get_physics_process_delta_time()

func set_mass(m):
	mass = m
	mu   = pow( m, -1.0 )

func _draw():
	draw_line(Vector2(),velocity, Color(0,0,0,0.2), 5 )
	draw_line(Vector2(),damping, Color(1,0,0,0.5), 3 )
