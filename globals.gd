extends Node

var stiffness:float = 0.85
var gravity:Vector2  = Vector2( 0.0, 1000.1 )
var gravity3d:Vector3  = Vector3( 0.0, -50.0, 0.0)
var medium_damping:float = 0.85
