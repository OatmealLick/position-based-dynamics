#tool
extends Spatial

# mouse drag params 
var selectionDistance := 10.0
var pressed           := false
var selection:PointMass3d
var prev_mouse_position
var distance_constraints: Array = []
var collision_constraints: Array = []

onready var camera := $"../Camera" as Camera

onready var node := preload("res://PointMass3d.tscn")   as PackedScene

export(int) var iterations := 1

class Constraint:
	var point1: PointMass3d
	var point2: PointMass3d
	var resting_length: float
	
	func _init(p1: PointMass3d, p2: PointMass3d):
		# assuming starting positions are resting length
		point1 = p1
		point2 = p2
		resting_length = point1.transform.origin.distance_to(point2.transform.origin) 
#		print("Created constraint of p1: %s, p2: %s, resting length: %s" % [point1, point2, resting_length])

class CollisionContraint:
	var entrypoint: Vector3
	var surface_normal: Vector3
	var node: PointMass3d
	
	func _init(_node, _entrypoint, _surface_normal):
		node = _node
		entrypoint = _entrypoint
		surface_normal = _surface_normal
	

func project_distance_constraints():
	for c in distance_constraints:
		project_distance_constraint_for_point(c)

func project_collision_constraints():
	for c in collision_constraints:
		project_collision_constraint_for_point(c)
		
func project_distance_constraint_for_point(c: Constraint):
	var p1 = c.point1
	var p2 = c.point2
	
	var wSum = p1.w + p2.w
	
	var n = p2.transform.origin - p1.transform.origin
	var d = n.length()
	var n_normalized = n.normalized()
	
	var delta_p: Vector3 = globals.stiffness * n_normalized * (d - c.resting_length) / wSum
	p1.predicted_position += (delta_p * p1.w * 0.5) / float(iterations)
	p2.predicted_position -= (delta_p * p2.w * 0.5) / float(iterations)
		
func project_collision_constraint_for_point(c: CollisionContraint):
	var vec = (c.node.predicted_position - c.entrypoint)
	var function = vec.dot(c.surface_normal)
	var gradient = c.surface_normal + vec
	
	var scaler = function / (c.node.w * gradient.length_squared())
	
	var delta_p = -scaler * c.node.w * gradient
	
	c.node.predicted_position += (delta_p) / float(iterations)
		
	
func _ready():
	if !Engine.is_editor_hint():
#		generate_sphere_body( Vector2(512,400), 50, 8 )
		generate_square_cloth( Vector3(-5, 5, 0), Vector2(20,9), Vector2(10,10) * 0.05  )
#		generate_line( Vector2(200,100), 30, 5 )


func _process(_delta):
	var base_velocity = Vector3(1, 0, -1).normalized()
	if Input.is_action_just_pressed("LMB"):
		for c in get_children():
			c.velocity = base_velocity * min(c.transform.origin.y, 1) * 5
			
	if Input.is_action_just_pressed("RMB"):
		for c in get_children():
			c.is_static = false
	
	# mouse drag
#	if Input.is_action_pressed("RMB"):
#		if pressed:
#			if selection != null:
#				selection.position          = get_global_mouse_position()
#				selection.previous_position = get_global_mouse_position()
#				selection.velocity          = Vector3()
#	if Input.is_action_pressed("LMB"):
#		if pressed:
#			for node in get_children():
#				if get_global_mouse_position().distance_to(node.position) < selectionDistance*selectionDistance:
#					node.position          += (get_global_mouse_position()-prev_mouse_position) * (1-get_global_mouse_position().distance_to(node.position)/(selectionDistance*selectionDistance))
#					node.previous_position += (get_global_mouse_position()-prev_mouse_position) * (1-get_global_mouse_position().distance_to(node.position)/(selectionDistance*selectionDistance))
#	prev_mouse_position = get_global_mouse_position()

func set_velo_from_external_forces(delta, node):
	node.velocity = node.velocity + node.w * globals.gravity3d * delta

func set_projected_position(delta, node):
	node.predicted_position = node.transform.origin + node.velocity * delta

func update_velocity_and_position(delta, node):
	node.velocity = (node.predicted_position - node.transform.origin) / delta
	node.transform.origin = node.predicted_position
	
		
func damp_velocity(node):
	if node.velocity.length() < 0.001:
		node.velocity = Vector3.ZERO
	else:
		node.velocity *= globals.medium_damping

func _physics_process(delta):
	collision_constraints = []
	for child in get_children():
		if not child.is_static:
			set_velo_from_external_forces(delta, child)
			damp_velocity(child)
			set_projected_position(delta, child)
			generate_collision_constraint_for_node(child)

	for x in iterations:
		project_distance_constraints()
		project_collision_constraints()

	for child in get_children():
		if not child.is_static:
			update_velocity_and_position(delta, child)


func generate_collision_constraint_for_node(node):
	var x = node.transform.origin
	var p = node.predicted_position
	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(x, p)
	if result:
		var q = result.position
		var n = result.normal
		collision_constraints.append(CollisionContraint.new(node, q, n))


func add_spring( node_1, node_2 ):
	pass
#	var link = link_object.instance()
#	link.initialize( node_1, node_2 )
#	$"../springs".add_child( link )

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			pressed   = true
			selection = null
			for node in get_children():
				var world_click_position = camera.project_position(event.position, 0.0)
				var raw_pos = event.position
				var from = camera.project_ray_origin(event.position)
				var to = from + camera.project_ray_normal(event.position) * 100
				to.z = 0
				
				if to.distance_to(node.transform.origin) < selectionDistance:
					selection = node
		else:
			pressed   = false
			selection = null
		

func generate_square_cloth( square_position:Vector3, dimensions:Vector2, spacing:Vector2 ):
	for y in range(dimensions.y):
		for x in range(dimensions.x):
			var new_point := node.instance()
			var other_point: PointMass3d
			var constraint: Constraint
			new_point.transform.origin = Vector3( x * spacing.x, y * spacing.y , 0) 
			new_point.transform.origin += square_position
			add_child(new_point)
			if x > 0:
				other_point = get_child(get_child_count()-2)
				add_spring(new_point, other_point)
				constraint = Constraint.new(new_point, other_point)
				distance_constraints.append(constraint)
			if y > 0:
				other_point = get_child(get_child_count()-dimensions.x-1)
				add_spring(new_point, other_point)
				constraint = Constraint.new(new_point, other_point)
				distance_constraints.append(constraint)
				if x < dimensions.x-1:
					other_point = get_child(get_child_count()-dimensions.x)
					add_spring(new_point, other_point)
					constraint = Constraint.new(new_point, other_point)
					distance_constraints.append(constraint)
				if x > 0:
					other_point = get_child(get_child_count()-dimensions.x-2)
					add_spring(new_point, other_point)
					constraint = Constraint.new(new_point, other_point)
					distance_constraints.append(constraint)
			if y==dimensions.y - 1:
				new_point.is_static = true
			

func _on_Gravity_value_changed(value):
	globals.gravity = Vector2(0, value)
	globals.debug.text = "Gravity = " + str(value)
	
func _on_Stiffness_value_changed(value):
	globals.stiffness = value
	globals.debug.text = "Stiffness = " + str(value)
	
func _on_Medium_value_changed(value):
	globals.medium_damping = value
	globals.debug.text = "Medium damping = " + str(value)
