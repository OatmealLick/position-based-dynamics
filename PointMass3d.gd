tool
extends Spatial
class_name PointMass3d

var velocity          := Vector3.ZERO
var predicted_position := Vector3.ZERO

var w := 1.0 # inv mass

onready var red = preload("res://RedMaterial.tres")

export(bool) var is_static := false setget set_static
func set_static(value):
	is_static = value
	if is_static:
		$Mesh.set_surface_material(0, red)
	
