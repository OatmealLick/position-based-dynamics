#tool
extends Node2D

# mouse drag params 
var selectionDistance := 10.0
var pressed           := false
var selection:PointMass
var prev_mouse_position
var distance_constraints: Array = []
var collision_constraints: Array = []

onready var link_object  := preload("res://Spring.tscn") as PackedScene
onready var point_object := preload("res://PointMass.tscn")   as PackedScene

export(int) var iterations := 1

class Constraint:
	var point1: PointMass
	var point2: PointMass
	var resting_length: float
	
	func _init(p1: PointMass, p2: PointMass):
		# assuming starting positions are resting length
		point1 = p1
		point2 = p2
		resting_length = point1.position.distance_to(point2.position)


class CollisionContraint:
	var entrypoint: Vector2
	var surface_normal: Vector2
	var node: PointMass
	
	func _init(_node, _entrypoint, _surface_normal):
		node = _node
		entrypoint = _entrypoint
		surface_normal = _surface_normal

		
func project_distance_constraint_for_point(c: Constraint):
	var p1 = c.point1
	var p2 = c.point2
	if p1.is_static and p2.is_static:
		return
	
	var wSum = p1.mu + p2.mu
	
	var n = p2.position - p1.position
	var d = n.length()
	var n_normalized = n.normalized()
	
	var delta_p: Vector2 = globals.stiffness * n_normalized * (d - c.resting_length) / wSum
	p1.predicted_position += (delta_p * p1.mu * 0.5) / float(iterations)
	p2.predicted_position -= (delta_p * p2.mu * 0.5) / float(iterations)
		

func project_collision_constraint_for_point(c: CollisionContraint):
	var vec = (c.node.predicted_position - c.entrypoint)
	var function = vec.dot(c.surface_normal)
	var gradient = c.surface_normal # + vec
	
	var delta_p = -function * c.node.mu * gradient
	
	c.node.predicted_position += (delta_p) / float(iterations)


func _ready():
	if !Engine.is_editor_hint():
#		generate_sphere_body( Vector2(512,400), 50, 8 )
		generate_square_cloth( Vector2(275,75), Vector2(6,8), Vector2(10,10) * 4  )
#		generate_line( Vector2(200,100), 30, 5 )


func _process(_delta):
	# mouse drag
	if Input.is_action_pressed("ui_accept"):
		for child in get_children():
			if child.is_static:
				child.is_static = false
				
	if Input.is_action_pressed("RMB"):
		if pressed:
			if selection != null:
				selection.position          = get_global_mouse_position()
				selection.previous_position = get_global_mouse_position()
				selection.velocity          = Vector2()
	if Input.is_action_pressed("LMB"):
		if pressed:
			for node in get_children():
				if get_global_mouse_position().distance_to(node.position) < selectionDistance*selectionDistance:
					node.position          += (get_global_mouse_position()-prev_mouse_position) * (1-get_global_mouse_position().distance_to(node.position)/(selectionDistance*selectionDistance))
					node.previous_position += (get_global_mouse_position()-prev_mouse_position) * (1-get_global_mouse_position().distance_to(node.position)/(selectionDistance*selectionDistance))
	prev_mouse_position = get_global_mouse_position()

func set_velo_from_external_forces(delta, node):
	node.velocity = node.velocity + node.mu * globals.gravity * delta

func set_projected_position(delta, node):
	node.predicted_position = node.position + node.velocity * delta

func update_velocity_and_position(delta, node):
	node.velocity = (node.predicted_position - node.position) / delta
	node.position = node.predicted_position
	
		
func damp_velocity(node):
	node.velocity *= globals.medium_damping

func _physics_process(delta):
		
	collision_constraints = []
	for child in get_children():
		if not child.is_static:
			set_velo_from_external_forces(delta, child)
			set_projected_position(delta, child)
			generate_collision_constraint_for_node(child)
			
	for x in iterations:
		project_distance_constraints()
		project_collision_constraints()
		
	for child in get_children():
		if not child.is_static:
			update_velocity_and_position(delta, child)
			damp_velocity(child)
			

func project_distance_constraints():
	for c in distance_constraints:
		project_distance_constraint_for_point(c)

func project_collision_constraints():
	for c in collision_constraints:
		project_collision_constraint_for_point(c)


func generate_collision_constraint_for_node(node):
	var x = node.position
	var p = node.predicted_position
	var space_state = get_world_2d().direct_space_state
	var result = space_state.intersect_ray(x, p)
	if result:
		var q = result.position
		var n = result.normal
		collision_constraints.append(CollisionContraint.new(node, q, n))


func add_spring( node_1, node_2 ):
	var link = link_object.instance()
	link.initialize( node_1, node_2 )
	$"../springs".add_child( link )

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			pressed   = true
			selection = null
			for node in get_children():
				if event.position.distance_to(node.position) < selectionDistance:
					selection = node
		else:
			pressed   = false
			selection = null


func generate_square_cloth( square_position:Vector2, dimensions:Vector2, spacing:Vector2 ):
	var points = []
	for y in range(dimensions.y):
		for x in range(dimensions.x):
			var new_point := point_object.instance()
			var other_point: Node2D
			var constraint: Constraint
			new_point.position = Vector2( x * spacing.x, y * spacing.y ) 
			new_point.position += square_position
			add_child(new_point)
			points.append(new_point)
			if x > 0:
				other_point = get_child(get_child_count()-2)
				add_spring(new_point, other_point)
				constraint = Constraint.new(new_point, other_point)
				distance_constraints.append(constraint)
			if y > 0:
				other_point = get_child(get_child_count()-dimensions.x-1)
				add_spring(new_point, other_point)
				constraint = Constraint.new(new_point, other_point)
				distance_constraints.append(constraint)
				if x < dimensions.x-1:
					other_point = get_child(get_child_count()-dimensions.x)
					add_spring(new_point, other_point)
					constraint = Constraint.new(new_point, other_point)
					distance_constraints.append(constraint)
				if x > 0:
					other_point = get_child(get_child_count()-dimensions.x-2)
					add_spring(new_point, other_point)
					constraint = Constraint.new(new_point, other_point)
					distance_constraints.append(constraint)
			if y==0:
				new_point.is_static = true
				new_point.mu = 1.0
	return points
			
func generate_square_body( square_position:Vector2, dimensions:Vector2, spacing:Vector2 ):
	var points = []
	for y in range(dimensions.y):
		for x in range(dimensions.x):
			var new_point := point_object.instance()
			var other_point: Node2D
			var constraint: Constraint
			new_point.position = Vector2( x * spacing.x, y * spacing.y ) 
			new_point.position += square_position
			add_child(new_point)
			points.append(new_point)
			if x > 0:
				other_point = get_child(get_child_count()-2)
				add_spring(new_point, other_point)
				constraint = Constraint.new(new_point, other_point)
				distance_constraints.append(constraint)
			if y > 0:
				other_point = get_child(get_child_count()-dimensions.x-1)
				add_spring(new_point, other_point)
				constraint = Constraint.new(new_point, other_point)
				distance_constraints.append(constraint)
				if x < dimensions.x-1:
					other_point = get_child(get_child_count()-dimensions.x)
					add_spring(new_point, other_point)
					constraint = Constraint.new(new_point, other_point)
					distance_constraints.append(constraint)
				if x > 0:
					other_point = get_child(get_child_count()-dimensions.x-2)
					add_spring(new_point, other_point)
					constraint = Constraint.new(new_point, other_point)
					distance_constraints.append(constraint)
	return points
			

func _on_Gravity_value_changed(value):
	globals.gravity = Vector2(0, value)
#	globals.debug.text = "Gravity = " + str(value)
	
func _on_Stiffness_value_changed(value):
	globals.stiffness = value
#	globals.debug.text = "Stiffness = " + str(value)
	
func _on_Medium_value_changed(value):
	globals.medium_damping = value
#	globals.debug.text = "Medium damping = " + str(value)
